﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(E4_BigSchool.Startup))]
namespace E4_BigSchool
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
