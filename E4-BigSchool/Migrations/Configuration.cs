﻿using E4_BigSchool.Models;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;

namespace E4_BigSchool.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<E4_BigSchool.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(E4_BigSchool.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.
        }
    }

<form>
<div class="form-group">
    @Html.LabelFor(m => m.Place)
    @Html.TextBoxFor(m => m.Place, new { @class = "form-control" })
</ div >
< div class= "form-group" >
    @Html.LabelFor(m => m.Date)
    @Html.TextBoxFor(m => m.Date, new { @class = "form-control" })
</ div >
< div class= "form-group" >
    @Html.LabelFor(m => m.Time)
    @Html.TextBoxFor(m => m.Time, new { @class = "form-control" })
</ div >
< div class= "form-group" >
    @Html.LabelFor(m => m.Category)
    @Html.DropDownListFor(m => m.Category,
    new SelectList(Model.Categories, "Id", "Name"), "", new { @class = "form-control" })
</ div >

public class CoursesController : Controller
{
    private readonly ApplicationDbContext _dbContext;
    public CoursesController()
    {
        _dbContext = new ApplicationDbContext(); -
    }
    // GET: Courses
    public ActionResult Create()
    {
        var viewModel = new CourseViewModel
        {
            Categories = _dbContext.Categories.ToList()
        };
        return View(viewModel);
    }
}

< div class= "form-group" >
    @Html.LabelFor(m => m.Category)
    @Html.DropDownListFor(m => m.Category,
        new SelectList(Model.Categories, "Id", "Name"), "", new { @class = "form-control" })
</ div >
< button type = "submit" value = "Save" class= "btn btn-primary" > Save </ button >

// GET: Courses
[Authorize]
public ActionResult Create()
{
    var viewModel = new CourseViewModel
    {
        Categories = _dbContext.Categories.ToList()
    };
    return View(viewModel);
}

@using(Html.BeginForm("Create", "Courses"))
{
    < div class= "form-group" >
        @Html.LabelFor(m => m.Place)
        @Html.TextBoxFor(m => m.Place, new { @class = "form-control" })
    </ div >
    < div class= "form-group" >
        @Html.LabelFor(m => m.Date)
        @Html.TextBoxFor(m => m.Date, new { @class = "form-control" | })
    </ div >
    < div class= "form-group" >
        @Html.LabelFor(m => m.Time)
        @Html.TextBoxFor(m => m.Time, new { @class = "form-control" })
    </ div >
    < div class= "form-group" >
        @Html.LabelFor(m => m.Category)
        @Html.DropDownListFor(m => m.Category,
            new SelectList(Model.Categories, "Id", "Name"), "", new { @class = "form-control" })
    </ div >
    < button type = "submit" value = "Save" class= "btn btn-primary" > Save </ button >
}

[Authorize]
[HttpPost]
public ActionResult Create(CourseViewModel viewModel)
{
    var course = new Course
    {
        LecturerId = User.Identity.GetUserId(),
        DateTime = viewModel.GetDateTime(),
        CategoryId = viewModel.Category,
        Place = viewModel.Place
    };
    _dbContext.Courses.Add(course);
    _dbContext.SaveChanges();
    return RedirectToAction("Index", "Home");
}

public class CourseViewModel
{
    [Required]
    public string Place { get; set; }
    [Required]
    public string Date { get; set; }
    [Required]
    public string Time { get; set; }
    [Required]
    public byte Category { get; set; }
}

public class CourseViewModel
{
    public string Place { get; set; }
    public string Date { get; set; }
    public string Time { get; set; }
    public byte Category { get; set; }
    public IEnumerable<Category> Categories { get; set; }
    public DateTime GetDateTime()
    {
        return DateTime.Parse(string.Format("{0} {1}", Date, Time));
    }
}

@model BigSchool.ViewModels.CourseViewModel
    ViewBag.Title = "Create";
    Layout = "~/Views/Shared/_Layout.cshtml";
}
< h2 > Create </ h2 >
< form >
    < div class= "form-group" >
        @Html.LabelFor(m => m.Place)
        @Html.TextBoxFor(m => m.Place, new { @class = "form-control" })
    </ div >
    < div class= "form-group" >
        @Html.LabelFor(m => m.Date)
        @Html.TextBoxFor(m => m.Date, new { @class = "form-control" })
    </ div >
    < div class= "form-group" >
        @Html.LabelFor(m => m.Time)
        @Html.TextBoxFor(m => m.Time, new { @class = "form-control" })
    </ div >
    < div class= "form-group" ></ div >
    < div class= "form-group" ></ div >
    < div class= "form-group" ></ div >
</ form >

Public class CourseViewModel
{
    public string Place { get; set; }
    public string Date { get; set; }
    public string Time { get; set; }    
}

< h2 > Create </ h2 >
< form >
    < div class= "form-group" >
        @Html.LabelFor(m => m.Place)
        @Html.TextBoxFor(m => m.Place, new { @class = "form-control" })
    </ div >
    < div class= "form-group" >
        @Html.LabelFor(m => m.DateTime) |
    </ div >
    < div class= "form-group" ></ div >
    < div class= "form-group" ></ div >
    < div class= "form-group" ></ div >
    < div class= "form-group" ></ div >
</ form >

<form>
    <div class="form-group">
        <label>
        <input class="form-control">
    </div>
</form>

@model BigSchool.Models.Course
@{
    ViewBag.Title = "Create";
    Layout = "~/Views/Shared/_Layout.cshtml";
}
< h2 > Create </ h2 >
< form >
    < div class= "form-group" >
        @Html.LabelFor(m => m.Place)
        @Html.TextBoxFor(m => m.Place, new { @class = "form-control" })
    </ div >
</ form >
}
